Was ist CEP?
+ Das verarbetieten von Event-Strömen um Schlüsse aus ihnen ziehen zu können
+ Bsp:
    + Recommendation-System basierend auf User-Events 
    + Börsen-lauf anhand von ttrading-daten 

Was ist ein Event?
    + Ein Nutzer clickt wohin
    + Sensor-Outputs
    + Finanzielle-Transaktionen

Was ist ein Event Object?
    + Ein Datensatz der ein Event repräsentiert
    
Unterschied zu DBMS
    + Daten werden nicht aktiv abgefragt
    + Alle Daten werden durch eine feste Abfrage-Reihe geschickt 
    
    
# Techniken => Bestimmt wichtig
+ Filtering
    + Filtern von relevanten Events
+ Pattern Matching
    + Finden von Event-Reihen die zusammengehören
    => GGf. neues event generieren
+ Value-Progression-Analysis
    + Ein bestimmter Wert wird über mehrere Events und ggf. einen Zeitraum überwacht
    + Wenn Schwellwert überschritten => Neues Event
+ Time-Progressiion-Analysis
    TODO: Direkter Unterschied zu VPA
    
+ Recursive Anwendung
    + Die Techniken generieren wieder Events, die wiederum Abgefangen werden können
    

# Produkte
+ APache Kafka und Flink
+ SAP ESP
+ Apama 
