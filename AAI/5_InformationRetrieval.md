Gesucht: Eine beliebige Informationen
Finden der Information aus einer (großen) Datensammlung
Ausgabe: Eine Menge von relevanten Dokumenten
=> Resourcen werden Dokumente geannt

=> Beispiel => Search-Engine

Kann eher Dokument-Retreivel genannt werden, weil ja kein Wissen, sondern nur Daten gesammelt werden.

# Main-Komponenten
    + Indexer
        + Erstellen von Indexen für Dokumente und verwaltung der Indexe
            => Lucene
    + Crawler
        + Extrahiert Daten aus Websiten
            => Nutch
    + Search Engine-Lib
        + Stellt Zugriff auf Indexer bereit
            => Lucene
    => Search Server Platform
        + Eigenständiger Prozess für Suchen
            => Elast Search
When to use what ?
    + Search-Engine 
        + wenn man einfach ne websearch einbauen möchte
    + Library
        + Wenn es gescheite für genutzte Sprache gibt (Java)
    + Platform
        + Stellt weitere Services bereit zu Administration
        => Leichter zu skalieren
        
# Quality of IR
+ False Negative
    + Relevant aber nicht geholt
+ False Positive
    + Geholt aber nicht relevant
+ Correct

Präzision = Anteil der Relevanten von allen geholten
    => Max = Einfach keine holen
    
Recall = Anteil der geholten Relevanten von allen Relevanten
    => Max = Einfach alle holen
    
F-Measure = Gesunde Mischung aus beiden, weil eines alleine nicht besonders gut ist

