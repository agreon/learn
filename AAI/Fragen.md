1 Intro

2 Knwoledge Representation
2. 
	Eine Klasse ist, ähnlich wie bei OO ein Oberbegriff der die Eigenschaften einer konkreten Entitäte beschriebt => Person
	+ Indivual konkrete Entität: Ich Lucas, Sie
	+ Relationship: is a Artist
	+ Rule: Eine Regel wie: Personen die Bilder gemalt haben sind Künstler


3. Onotology
	=> Vorteil zu Relational-DB ist, dass auf Bezeihungen unter Entitäten wesentlich leichter zugegriffen werden kann
	=> Machinelles Reasoning wird stark verleichtert

4. Approaches to Knowledege Representation?
	TODO:
		+ Prädiakten-Logik
		+ Frames
		+ Semantic-Nets
		+ Rules

5
	+ Das ein Programm Schlüsse aus gegebenen Daten zieht und so neues Wissen generiert.
	+ Bsp: Personen die Bidler gemalt haben..
	+ Mit Ontology?

6 OWA vs CWA
	+ Alle relevanten Daten sind eingetragen, Museumsbeispiel
	+ bei OWA ist diese sicherheit nicht gegeben. 
	=> Wirkt sich auf die Beantwortung von logischen Fragen auf das System aus
	=> Artist-Beispiel

7 Was ist eine Resource in RDF?
	+ Alles was für die aktuelle Aufgabenstellung relevant sind.
8 Namespaces bündeln mehrere Resourcen
	+ definiert die volle URI
9 
	+ Stellt eine beziehung von RDF-Resourcen dar
	+ Gegliedert in Sobjekt, Prädikat, Objekt

10 
	+ rdf:Person rdf:type rdf:Class .
	+ rdf:Daniel rdf:type rdf:Person .	

11 Die vorgeschlagenen Attirbute sind nicht bindend TODO
	+ No Instance without Class
	+ Kein Type-Checking

12 rdf:IsA rdf:type rdf:Property . 
rdf:Daniel rdf:isA rdf:Person

13
	+ Jeder kann alles Ändern
	+ Jeder kann alles über jedes Thema sagen.
	=> Daten können falsch und unvollständig sein => OWA

14
	+ Tortoise
	+ XML
	+ n-Triples


3 Queries TODO
1 Tabelle mit Spalten für jede angeforderte Resource
2 Path Expressions
3 Transitive Closure
4 Additional Features
5 KR-Services
6 KR-Products => 
7 Product and Service Maps
	+ zuerst stellt man eine Service-Map für die geplante App auf
	+ Man identifiziert Kompoentnen die notwendig sind
	+ Dann entscheidet man sich mit hilfe der Product map für services
	=> Falsch TODO
	
# 4 Arhcitecture
Referenz-Architektur: Ein Bauplan für eine Architektur der häufige Problemstellungen bei Software-Entwicklungen abdeckt

+ Main-Components: 
	+ Frontend
	+ Backend / Controller
	+ Datenbank

+ Relevante Technologien
	+ Frontend: HTML/React usw
	+ Backend: Java mit Rest-Frontend wie Spring. Verbindung zu RDf-Store mit jena
	+ Datenbank: Ontology => Falsch: TODO
		+ rdf4j

TODO: Oft genutzte => 
	+ 

# 6 Information Retrieval
1 What does IR mean?
	+ Das bescahffen von relevanten Informationen / Dokumenten von einer Menge Dokumenten
2 
	+ Am besten mit dem F-Measure: Präzision und Recall sind beide alleinstehend nicht seht gut. Wenn man Präzision maximiert, erhält man keine Dokumente und wenn man den Recall erhöht alle.
	=> Das F-Measure stellt eine Gleichgewicht aus beidem dar.
3 
	Main-Services: 
		+ Crawler
			+ Durchforstet Webpages nach Infromationen (Suchmacshine)
		+ Indexer
			+ Indexed die gefundenen Informationen
		+ Library die beides enhtält und Zugriff gewährt
	Framework
		+ Bietet ein eigenständige Suchmaschine an, die mit eigenen Indexen gefüllt werden kann.
	Service
		+ Google

4 When to use what
	+ Service wenn nur simple websuchen ermöglicht werden sollen
	+ Sobald konkreter Anforderungen and ie Suche stehen, muss gezwungenermaßen auf ein Framework zurückgegriffen werden.
	+ Falls die Konfigurierbarkeiet dann immer noch nicht ausreicht = Library

5 Technologies 
+ Service= Google
+ Frameworks = Elastichsearch
+ Library = Apache Lcuene

# 7 Agents
1 Was ist ein Agent
	=> Ein Programm, dass seine Umgebung wahrnimmt und basierend auf der Wahrnehumung Aktionen ausführt
	+ Bsp:
		+ Mensch
		+ THemostat
2 Was unterscheidet Complexe Agenten von simplen?
	+ Complexe Agenten lernen aus Fehlern und wissen / Können Abshcätzen welche Auswirkungen Ihre Aktionen auf die Umgebung haben

3 What services do frameworks for building complex agents offer?
	+ DSL 
	+ Plugin Architektur ird oft verwendet

4 Wie sollte eine Agenten-Logik implementiert werden?
	+ Stirkte entkopllung
		+ Aktoren und Sensoren
	+ Am besten Plugin-basiert
	=> So kann die Buisness-Logik von der Wissensbasis abgekoppelt werden	

# 8 NLP
1 
	+ Translation
	+ Das generieren von Menschlich-Verständlichem Text
	+ Das extrahieren von Informationen aus Texten
	+ Das verstehen von Texten für bsplw. Chatbots
2 7  Levels
	+ Acoustische Signale
	+ Laute
	+ Buchstaben
	+ Wörter
	+ Sätze
	+ Wissen
	+ Aktion

3 
	+ Tokenizing: Herauslesen der einzelnen Wörter aus einem Text.
		=> Aufteilen des Textes in Wörter aber auch Satzzeichen
	+ Sentence-Splitting: Anhand der gefundenen Satzzeichen und Wörter wird der Text in Wörter aufgeteilt.
	+ POS-Tagging: 
		+ zuweisung der Wörter ihrere Grammatischen Klassen. 
		=> verben, nomen usw.
	+ Parsing
		+ Mithilfe einer definierten Grammatik wird ein Parse-Tree aufgebaut, mit dem im Anschluss weitergearbeitet werden kann.

4 
	+ Eine art Wörterbuch stellt für Wörter Synonyme und Beziehungen zu Annderen Wörter zur Verüfgung
	+ Examples
	=> WordNet / GermaNet
	
5 What do Frameworks Offer?
	=> Eine Pipeline mit all den vorgestellten Komponenten
	+ Die Resourcen, sowie Parser, Tokenizer usw..

6 Web Services
	+ Man benötigt keine direkte Implementierung in der eignenen Anwendung
	=> Man kann web services über eine API ansprechen.
	+ Dandelion stellt bsplw. NER bereit. 
		=> Finden von Entitäten in Sätzen
	+ Google-Translate übersetzt texte

# 8 CEP
1 What is CEP
	=> Das verarbeiten eines Event-Streams um Informationen aus einer großen Menge an Daten zu gewinnen
2 What is a event and a event-object
	+ Ein event beschriebt ein belibeges Ereignis wie z.b. das eintreffen Eines Zuges, der Click eines nUtzers
	+ Das Objekt ist ein konkreter Datensatz für ein bestimmtes Event
3 vs DBMS
	+ Bei CEP werden Queries nicht aktiv an den Datenstand gestellt
	+ Die Daten druchlaufen vorher definierte queries
	+ => Queries permanent, Daten nich
4 Prominent CEP
	+ Apache kafka
		=> CEP-Framework für Standard-Tasks
	+ Apache flink
		=> Konkurrent
# 9 ML
1 Was ist ML?
	+ Das bilden eines Modells mit dem vorhersagen getroffen werden können
2 Applications
	+ Finden von Kunden-Gruppen
	+ Finden von ähnlichkeiten und mustern in Daten
	+ vorhersage (Maschinen-kaputt)

3 
	Supervised
		+ Beim supervices learning wird zur Modellbildung ein Trainingsdatensatz verwendet. Dieser enthält das gewünschte Ergebnis und so wird ein modell aufgebaut, dass versucht die Problemstellung zu generalisieren.
		+ Supervised, weil die Ergebnisse bekannt sind und so die Akkurarität des Modelss evaluiert und auf Basis dessen verbessert werden kann
	Unsupervised
		+ Dort werden Daten verwendet, die noch keine Zielvariable besitzen
		+ Wird zum Beispiel beim Klustering verwendet, um Datensätze Gruppen zuzuordnen
	Reinforcement TODO
		+ Das Modell wird iterativ verbessert, indem Informaitonen
	Klassifizierung
		+ Dabei wird versucht ein Datensatz einer diskreten Menge an Klasssen zuzuordnen. Bsp: Objekterkennung auf Bildern
	Regression
		+ Für Datensätze wird ein (Numerisch/kontinuierlicher) möglicher Output errechnet. 
	Klustering
		+ Es wird versucht, Datensätze anhand ihrer Features zu Gruppieren
	Feature-Selection
		+ Das finden von für ML relevanten Features.. Name ist z.b. irrelevant für Überleben
	Topic Modeling
		+ Mit Hilfe von NLP auslesen von Topics von Dokumenten. Klassifikation  der Dokumente nach Topics.
	 
4 
	Decision Tree
		Auf Basis der Trainingsdaten wird ein Baum aufgebaut. Jeder Blattknoten stellt eine Ausgabe-Klasse dar. An allen anderen Knoten geschieht ein sogenanter Split, der auf den jeweiligen Atributen etnschieden wird. => Information gain
	NN
		Neural Networks sind dem Menschlichen Gehirn nachempfunden.	Ein NEntwork besteht aus mehreren Layern (IN/Out/Hidden)
		Jeder Layer beseitzt eine ANzahl von Neuronen. Diese enthalten wiederum eine Anzahl von Inputs anderer Knoten, die gewichtet sind.
		Außerdem existiert eine Aktivierungsfunktion. Es werden alle Inputs zusammengezählt und anhand der Funktion etnschieden ob der Output des Knotens feuert. 
		Missing: Die Gewichte werden wöhrend der Lern-Phase angepasst
	Bayesian Networks TODO
		Wird als Graph dargestellt
		Jeder Knoten stellt einen möglichen Zustand dar.
		An jedem Konten sind die wahrscheinlichkeiten eingetragen, mit deren Wahrscheinlichkeit der Zustand des Knoten eintritt. => Label
		=> Domain-Knowledge gebraucht
	Deep Learning
		Es werden meist mehere NN und unsuperviced techiken verwendet. zum Beispiel in der CV viel verwendet.  
	Induktive Logic Programming
		Hardocidertes einprogrammieren von Regeln in das System.
		Die Regeln müssen konsistent sein.
		Anhand der Regeln kann daS system schlüsse für eingabedaten ziehen
5.
	Training und Testdaen müssen zu Beginn gesplitet werden (meist 25%)
	Mit den Trainingsdaten wird ein Modell gebildet.
	Anschließen wird der Zielwert aus den Testdaten entfernt und das Modell predicted den jeweiligen Zielwert für alel Datensätze
	Zuletzt können die geschätzten und wirklichen Zielwerte verglichen werden um die Qualität des Modells zu evaluieren
6 
	Overfitting beschreibt den Fall, dass das Modell zu sehr an den Trainingsdatensatz angepasst ist. Dann können neue Datensätze nicht gut bearbeitet werden.
	Beispiel ist, wenn alle Datensätze gemerkt wurden. 
	Das Modell generalisiert die Problemstellung nicht genug.
7 Tools
	RapidMiner
	Tensorflow for NN
	Scikitlearn for python-ml

# 10 CV
1 Applications
	=> Objekterkkenung auf Bildern
	=> Gesichtserkennung
	=> Aber auch generierung von bildinformationen => Füllen von Löchern
2 Main Tasks
	=> Bildaufnahme
	=> Bildkorrektur
	=> Feature-Extraktion
	=> Segmentierung
	=> Objekterkennung
	=> Generierung 

3 Libraries
	=> Google Vision API
	=> OpenCV gibt CV-Algorithmen
	=> 
4 Adv/Dsiv
	=> Advantages
		+ Man muss selbst nichts aufsetzen außer APi-Calls
	=> Disadvantages
		+ Lizenzen müssen geklärt sein (Wie viel Requests)
		+ AUßerdem eine Frage der Availability
		+ Nicht perfekt auf Use-Case angepasst

5 Wofür und wie wird die Softmax-Funktion verwendet?


6 Cross Entropy
