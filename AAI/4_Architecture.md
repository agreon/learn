Architektur hängt ganz vom Use-Case ab.
AI-Applikationen ähnlich zu Buiseneess-Applikationen
=> Software-Engineering Methoden anwendbar

Referenz-Architektur: Vorlage einer Architektur für einen konkreten Use-Case 

Typischer Aufbau:
    + Presentation-Layer (Frontend)
    + Applikationen-Logik (Main-Backend/Controller)
    + Knowledge-Base - Engine 
    + Datenbank / Ontology

TODO: Main-Technologies 
+ RDF4j
+ Jena 