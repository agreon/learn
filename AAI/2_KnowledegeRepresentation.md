Alle KI-Applikationen benötigen Wissen für ihre Arbeit

# Onotology
+ Representation von Wissen aus einer bestimmten Domäne
+ Representation von relevanten Konzepten und Ihrer Beziehung untereinander
+ Enthält
    + Schema
        + Spezifiziert den Aufbau der Ontology und legt fest wie das Wissen dargestellt wird. 
        + Klassen
            + Beschreibung gemeinsamer Eigenschaften zur Einordung von Entitäten
        + Mögliche Beziehungs-Typen
            + is a , created by
        + Regeln
            + Bsp: Jede Person, die ein Bild gemalt hat, ist ein Künstler.
            => Helfen beim Schlüsse ziehen
    + Fakten
        + Basieren auf dem Schema
        + Konkrete Entitäten und Beziehungen unter den Entitäten
        + Individuen
            + Instanzen von Klassen
        + Beziehungen
            + Konkrete Beziehungen unter den Individuen
+ Es ist leichter Wissen aus Onotologys als aus Relationalen Datenbanken zu beziehen
=> Man kann Fragen mit der Hilfe der Ontology beantworten

# Representation-Approaches
+ Predikaten-Logik
+ Rules
+ Semantische Netze
+ Frames

# Open vs Closed world assumption
+ Closed-World
    + Die Datenbank ist komplett
        + Wenn man z.b. nur ein bestimtmes Museum abbildet
    + Alles was nicht gefunden wird, existiert nicht 
+ Open World 
    + wie viele gemälde hat A gemalt?
        => Mindestens 1
TODO: 10 , CWA vs OWA Übung

# RDF
+ Herangehensweise zur Formulierung logischer Aussagen
+ Resource
    + Alle möglichen Einheiten 
    + Identifiziert durch URI
+ Namespace: Bündelt mehrere Resourcen
+ Jede Aussage besteht aus drei Einheiten: Subjekt, Prädikat und Objekt (Triple)
    + Objekt => Resource oder Literal
    
+ Typ : is-a Beziehung 
+ Class: Meta-Klasse 
    + Person ist eine Klasse
    => dbpedia:person rdf:type rdfs:Class.
    + Unterschied zur Objekt-Orientierung:
        + Es kann keine Instanz ohne eine Klasse existieren
        + Eine Instanz gehört zu nur einer Klasse
        + Klasse spezifiziert alle Attribute und Methoden einer Instanz
         
     
+ Property: Definition eines Prädikats
    + movement rdf:type rdf:Property.
    + Unterschied zu OO:
        + OO ist viel strikter, type-checking usw.

# Service and Product Maps
+ Genutzt um geeignete Produkte für eine (AI)-Applikation zu finden

+ Vorgehen
    + Erstellen einer Service-Map mit relevanten Services
    + Auswählen von einer Reihe möglicher Kandidaten (aus der Product Map)
        + Anhand von Kriterien wie z. B. der Programmiersprache, Umgebung usw.
        
Product Map: Service Map nur mit konkreten Produktvorschlägen

# TODO: Service und Produkt-map für Knowldege-Representation
