# Aufgabenfelder
+ Verstehen, Generieren von Texten
+ Extrahieren von Inhalten
    => Um IR zu betreiben
+ Zusammenfassen von Texten 
+ Übersetzen

# 7 Levels of Language Understanding
+ Signale
+ Sounds / Geräusche
+ Buchstaben
+ Wörter
+ Sätze 
+ Wissen 
+ Konsequenzen
+ TODO: ggf. Techniken dazu 

# Building-Blocks

##  Tokenization
+ Einzelne Wörter aus einer Zeichenkette extrahieren. (Token)
    + Zusammenfassen von Buchstaben 
## Sentence splitting
+ Aufteilen eines Textes in Sätze.

## POS-Tagging
+ "Part Of Speech"
+ "Reduzieren des Wortes auf seine Wurzeln"
+ Kategorisieren von Wörtern bezüglich der Grammatikalischen Kategorie
=> Bsp. Verb, 3. Person Singular

## Parsing
+ Analysieren der Grammatik
+ Aus den Tokens und einer Grammatik wird ein Parse-Tree generiert
=> Am besten schneiden statistische ab.

# Service-Map
+ Services 
    => Meist reichen Web-Services aus (Aber Lizenz und Availability checken)
    + Googl Transalte, Dandelion
+ Frameworks 
    + Meist wie Pipeline aufgebaut, mit austauschbaren Komponenten
    => Für Komplexe Anwendungen gedacht 
    => Uima, Gate
+ Building Blocks, Language-Resources 
    + Language-Resources: Dict mit Synonymen und Verwandtschaft
    => Sehr selten notwendig selbst zu entwickeln
    => Stanford Tokenizer, WordNet
    