What does Artifical Intelligence mean?
=> Software that exhibits Human behaviour
    + Fühlen (Sehen, hören, mechanisches fühlen)
    + Reason/Schlüsse aus dem gewonnen ziehen: Lernen, Planen
    + Kommunizieren: Sprechen, schreiben
    + Aktion: Basierend auf den gewonnen Informationen

Areas
+ Computer Vision, NLP, Machine Learning,     

# History
+ Origin (50-60)
    + Erste Erwähnung in 1956 im Dartmouth Workshop (Claude Shannon)
    + Turing hat mit Turing-Test 
+ Hype (60-80)
    + Bekannte Forscher haben den Hype vorangetrieben
    + In den nächsten Jahren wird das Problem der KI gelöst sein
    + Resultierte in Viel Geld für die Branche
+ Winter (80-90)
    + Ernüchterung, weil die Versprochenen Ziele nicht erreicht wurden.
    + Die Fundings wurden gestrichen und der KI-Markt bricht zusammen
+ Aktuell
    + Private Unternehmen haben durch Anforderungen des Marktes die Entwicklung von KI wieder aufleben lassen
    + AI findet verwendung in vielen Tagtäglichen Produkten
        + Sprachsteueruzng, Gesichtserkennung, AI in Computerspielen
    
Begriff: Hype-Cycle
    + Zu Beginn krasse Vorstellung
    + Dann ernüchterung
    + Letztendlich stabilisierung durch realisitischers herangehen
    
# Prominente Projekte
+ 1997 Deep-Blue schlägt den Schach-Weltmeister
+ 2011 IBM Watson gewinnt Jeopardy
+ 2016 Google Deepmind schlägt Go-Champion
+ Selbstfahrende Autos, Fußball-Roboter

# Areas
+ Knowledege Representation
    + Wie soll Wissen repräsentiert/gespeichert werden
+ Agents