Was ist Machine Learning?
+ Automatisierte Generierung eines Modells auf Basis von Lern-Daten um mit dem Modell Vorhersagen zu treffen
+ Besonderheit: Keine explizite programmierung von Regeln notwendig; Programm findet Muster selbstständig

# Beispiele
+ Spam-Filtering
+ Stock-Market Analysis und Prediction
+ Recommendation

# Gebiete
Supervised
+ In den Lern-Daten ist auch der gewünschte Output enthalten um zu validieren wie gut das MOdell ist und es ggf. zu verbessern
=> Ziel: Lernen des In to Output Mapping
    + Klassifikation
        + Zuweiseung von Klassen für Inputs
    + Regression
        + Zuweiseung von diskreten Werten für Inputs 
Unsupervised
+ Pure Eingabe-Daten 
=> Ziel: Finden von Strukturen in den Daten 
    + Klustering
        + Finden von ähnlichen Datensätzen und Zusammenfassen dieser
    + Feature-Selection
        + Finden von Attributen eines Objektes, die gut für die Klassifikation/Regression gebraucht werden können
Reinforcement-Learning 
+ Ein Agent muss eine Lösung in einer dynamsichen Umgebung finden
=> Ziel: Verbessern des Agenten 

# Ansätze
+ Decision Trees
    + Für supervices-learning genutzt
    + Basierend auf Inputs und ihren Attributen wird ein Baum aufgebaut
        + 
    + Neue Werte werden anhand der Splits klassifiziert/regressiert
+ Neurale Netzwerke
    + Dem Menschlichen Gehirn Nachempfunden
    + Netz aufgebaut aus mehreren Layern von Neuronen
    + Neuronen haben mehrere gewischtete Inputs und einen Treshold, mit dem sie Feuern
    + In der Trainingsphase werden die gewischte an den Inputs angepasst
    + In der Recognitionphase werden AUsgaben für Datensätze generiert
    
+ Deep-Learning
    + Meist verwendung von NNs
    + Mehrer Layer mit verschiedenen Aufgaben
    + Feature-Selection und Klassifikation vereint
    
+ Bayesian Networks TODO
    + Directed-Acyclic-Graph

    => Vorteil: Es kann heruasgelesen werden, wie das Programm auf die Ausgabe-Daten gekommen ist 
        => Aber Domain-Knowledege notwendig
    
+ Inductive Logic Programming
    + Gegeben: Eine Menge von Fakten
    + Ziel: Generierung von konsistenten Regeln, die mit den Fakten übereinstimmen

# Methodikt
+ Training/Testing
    + Trainings und Testdaten müssen zu Beginn strikt getrennt werden 
    + Anlernen des Modells mit dem Trainingset
    + Testen des Modelss mit Testset ohne Output-Value
    + Vergleich der Ausgabedaten (MAE)
+ Overfitting
    + Modell ist zu gut an Trainings-Daten angepasst 
    + "Erinnerung von allen" ist ein gutes Beispiel für Overfitting
    => Genug abstrahierung notwendig


#TODO: Kompletter Prozess: Skript 72
    
    
# Service Map 
+ Services 
    + Google Prediction API
+ Frameworks
    + Rapid-Miner
+ Libraries
    + Mahout?
    + Tensorflow