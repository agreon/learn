# Übersicht

# Techniken

- 1 - Einführung
    - [ ] Map-Reduce?
    - [ ] Greenwald-Khanna Algorithmus
    
- 2 Data at Rest
    - [ ] k means -> k-means++ -> k-means ||
        - Nicht: Gütegrenzen kennen
    - [ ] Klassifikation => 
        - [ ] Naive Bayes
        - [ ] Entscheidungsbäume
        - [ ] Ensemble Methoden 
		- [ ] Random forest
        - [ ] Dimensionsreduktion 
- 3 Data in Motion
    - [ ] Windows? 
    - [ ] Basic Counting 
    - [ ] Count Min Sketch
- 5 Graphen 
    - [ ] Eigenschaften berechnen
        - [ ] Adjazenzmatrix
        - [ ] Degree Centrality (global)
    - [ ] Zusammenhangskomponenten finden 
    - [ ] Kluster-Koeffizient
        - [ ] Gesucht: Kluster-Koeffizient von 1/2!
        - [ ] Globaler Koeffizient
    - [ ] Transitionsmatrix mit Teleporation (Pagerank)
    - [ ] Community-Detection
          
# Knowledege
- 1 - Einführung
    - [ ] 10 Data Science Mistakes!?
    - [ ] Vertical vs. horozontale Skalierung
    - [ ] Generationen von Machine-Learning Tools
    - [ ] Map Reduce
    - [ ] Phasen von KDD, CRISP-DM, data science proczess
        - Explorative Datenanalyse
    - [ ] (Approximatives) Binning 

- 2 Data at Rest
	- [ ] Ellbow
	- [ ] Cross Validation
    - Nicht
        - PMML vs PFA

- 3 Data in Motion
    - [ ] Herausforderungen + Anforderungen
    - [ ] Technologische Konzepte
    - [ ] Grundlagen
        - ....
        - [ ] Windows
        - [ ] Verarbeitungsgarantien
        - [ ] Zeitmessung
    - [ ] Verfahrenskategorien
        - [ ] Deskriptiv
        - [ ] Sampling
        - [ ] Sketching 
    - [ ] Modellbilung und Evaluation
        - [ ] Online vs. Offline
        - [ ] Very fast descicion tree 
        - [ ] One-shot vs one-pass
    - [ ] Concept-Drift
    - Nicht 
        - Model Persistence 
        - Micro-Clustering
- 4 Referenzarchitekturen
    - [ ] Unterschiede Data at Rest <=> Data in Motion
        - [ ] CAP Theorem
        - [ ] Anforderungen
    - [ ] Lambda Architektur
    - [ ] Kappa Architektur
- 5 Graph Analysis
    - [ ] Eigenschaften
    - [ ] Muster 
    - [ ] Generierung?
    - [ ] Pagerank
    - [ ] Dimensionsreduktion?
    - Nicht 
        - PCA
        - Poweriteration

# Blatt?
- Formeln?
