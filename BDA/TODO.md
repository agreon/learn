## 1

# Inhalt
+ Vertikal vs horizontal; tool-generationen
+ map-reduce
+ binning

# TODO

Generationen
+ Vor- und Nachteile!TODO

+ Wofür Binning
	+ Einteilen von Datensätzen anhand eines Merkmals in entweder gleich große Mengen oder durch ein minimum und maximum begrenzte bestimmte Mengen.
	=> Klassenzuordnung mit der weitergearbeitet werden kann
	+ Darstellung der Häufigkeitsverteilung einer Menge mit einem Histogram.
	=> Auch diskretisierung

+ Wann quanitl-binning?
=> Um die Verteilung der Daten zu finden
=> Um den Dominanten Wetebreich zu finden?

+ Welche Gefahr entsteht bei den Bin-Grenzen?
=> Werte an den Grenzen können in mehreren Bins landen
	=> TODO: Warum?

+ Wie oft muss der Datensatz durchlaufen werden
	+ Bei Bins gleicher Breite 
		+ 1x => Finden von min. und max.
	+ Bei Bins gleicher Größe
		+ Oft, da sortierung notwendig

+ Ist es sinnvoll Binning auf Stichproben durchzuführen?
	+ Nein, weil min und max nicht gefunden werden sowie die verteilung der Daten nicht repräsentiert wird


## Greenwald Kannah 
=> Wir müssen uns nicht alle Elemente merken, um eine approximative Anfrage beantworten zu können

+ Was ist ein approximatives Quantil
=> Ein Quanitl mit einem möglichen, definiertern Fehler.

+ Wie wirkt der Error €?
=> Er ist die erlaube Abweichung vom q(Quantil) (q-€)/(q+€)

+ Warum darstellungsart nicht mit min. und max.
	=> Weil bei einer Änderung der Liste alle Quantile geupdated werden müssen

+ Summary für jeden Knoten
    => Eine globale Summary aus mehreren
        => Nur unter bestimmten Qualitätseigenschaften!!
            => Wann können die verschmolzen werden?
	    	=> Wenn der akzeptable Fehler eingehalten wird!
		=> gj ... gi + gi+1 h deltai+1 < 2€n


    
Wofür MapReduce
+ schnelle Verarbeitung von sehr großen Datenmengen durch parallelisierung 
=> TODO: Median berechenbar?
	=> Nein?

## 2

# Inhalt
+ Clustering
+ Klassifikation
	+ Naive-Bayes
	+ Decision Trees
	+ Dimensionsreduktion

# lernen

deskriptiv vs. prädiktive Modellierung
+ Deskriptiv: Finden von Mustern im Datensatz => Modellbildung
+ Prädiktiv: Zuordnen von Datenpunkten zu bestehenden Klassen

Warum ist k-klustering einfach parallelisierbar?
+ Ein Knoten für jeden Zentroiden, der die Distanzen berechnet 

Problematik von K-means++?
=> Datensatz muss k x durchlaufen werden, um die initialen zentroiden zu finden

Wie wird die skalierbarkeit durch k-means || erreicht?
=> Es werden merhere neue Punke(l) in einem Schritt berechnet, anstatt 1x pro Schritt.

Was ist die Elbow-Methode?
+ Man führt eine Clusteranalyse für eine bestimmte größe k druch
+ FÜr jedes k wird der quadratische Fehler berechnet
+ aus den resultierenden Daten kann abgeleitet werden, ab welchem k der Fehler konvergiert und es sich nicht mehr wirklich lohnt k zu erhöhen.

Warum wird der Fehler bei größerem k fast immer geringer?
	=> Warum? 
		=> Warhscheinlichkeit, dass knoten in der nähe eines Zentroiden liegt, nimmt zu
	=> Warum nicht?
		=> wenn daten stark verteilt sind (viele ausreißer), dann werden zentroiden an stellen gesetzt, die dazu führt, dass die mae zunimmt

Wie funktioniert Cross Validation im Clustering?
	+ Aufteilung in m partition.. m-1 wird geclsuterd
	+ m. partition wird getesetet
	=> Für sehr viele k testen => bestes k gewinnt

2 - klassifikation
# TODO



+ Wie funktionieren Entscheidungsbäume?
=> Bei jedem Split wird das wichtigste Merkmal ermittelt, dass die reinsten Folge-Partitionen erzeugt.

+ Wie wird dieses Merkmal ermittelt?
	=> Information Gain = next entropy - Current Entropy 
	+ Entropy => Reinheit der entstehenden Partition
	+ Gini-Index => 

+ Wie wird dies durchgeführt?
	+ Greedy:
		+ Such nach lokalem optimum
		+ Kein Backtracking
		+ wenn später festgestellt wird, dass was besseres existiert.. egal
		=> Schnell
	+ Kategorische
		+ n-äre splits
		+ teilmengen binär
	+ Wie arbeitet man mit kontinuierlichen merkmalen?
		=> Binäre splits	
		=> Binning
	+ Was sind die Aufwände bei diesen Strategien? (56)
		+ 3 <=> k ?

+ Wie werden Splits auf großen Datenmengen entschieden?
	=> Problem: Sortierung notwendig => Nicht möglich
	=> Diskretisierung der Daten
		+ Binning mit Greenwald-Khanna
	+ TODO: Blatt? => Sufficient Statistics (61-62)
		+  TODO TODO


+ Wie wird Overfitting vermieden bei Decision Trees?
	+ Frühzeitiges stoppen des aufbaus 
		=> Depth-Parameter
		=> No furhter infromation gain
		=> Min-Instances per node
	+ Pruning	

+ Erklären sie den zusammenhang von Genauigkeit und Robustheit
	+ Genauigkeit => Möglichst viele korrekte Klassifikationen auf den Trainigsdaten
	+ Robustheit => Möglichst gleiche Genauigkeit auf einem Testdatensatz
	+ Wenn die Genauigkeit maximal, aber dafür die Robustheit nicht => Overfitting


+ Was sind Methoden zur vermeidung von Overfitting im Allgemeinen?
	+ Trainings und Testdaten aufteilung
	+ Strichproben-Verfahren (Sampling)
	+ Ensemble-Methoden
		+ Kombination von mehreren Modellen um allgemein gutes zu bekommen
		=> Majority Voting der Modelle entscheidet

+ Stichproben: Bootstrapping
	=> Für eine beliebige anzahl an Durchgängen werden d Strichproben mit zurücklegen gezogen
	=> So existiert beim nächsten Durchgang die Möglichkeit, dass Datensätze nochmal für Training verwendet werden
	=> TODO2: VOrteile (Varianz/Bias)?

+ Bagging (Bootstrap-Aggregation)
	=> Für jedes Modell wird die Bootstrap-Methode verwendet um Trainingsdaten zu erhalten

+ Warum ist ein Bagged Ensemble Klassifier robuster gegenüber verrauchten daten und overfitting?
	=> Durch die Kombination der einzelnen Modelle wird die Varianz und Overfitting vermieden.

+ Warum vermeiden Random Forests overfitting?


+ Warum müssten Random Forests weniger Merkmale pro Split betrachten?
	
+ Was bewirkt eine kleinere/größere Auswahl von Prädikatoren bei Random Forests?(29)
	größer 
		=> overfitting
	kleiner 
		=> Schnellere Berechnung
		=> underfitting?


+ Erklären sie data-leakage

	 
+ Was ist der Fluch der Dimensionalität?
=> Mehr dimensionen => Weniger ähnlichkeit => Mehr ausreißer
	=> Letzendlich stellt jeder Punkt für sich einen Ausreißer da.
=> Seite 2 => Doch ähnlicher? hä
	+ Mehr Informationen die vermutlich irrelevant / duplikate sind
	+ Die Daten werde sich immer ähnlicher
	+ Daher Dimensionreduktion notwendig

+ Dimensionsreduzierung (Selection/Extraction)
	+ Selektion
		=> Filtern von Features
	+ Extraction
		=> Bilden von neuen Feautres anhand von vorhandenen attributen

+ Was ist ein latenter Faktor?
	+ Ein unabhängiges Merkmal das mehrere Datensätze beschreibt?
	+ Ein Merkmal, dass mehrere Merkmale eines Datensatzes ersezt.

# Notes

+ Forest - Random Merkmale
+ Für jeden Baum gleiche Traindaten aber mit zufälligen Prädikatoren

+ Forest - Random Input selection
=> Für jeden Baum jeweils ein zufälliger Datensatz mit Bootstrapping generiert
=> Für jden Baum wiederum zufällige Prädikatoren gewählt
=> Am Ende keine Pruning und keinne max-depth

+ Forest - Random Linear Combinations
=> Geeignet, wenn insgesamt wenige Prädikatoren zur Verfügung stehen

# hilfszettel?
+ Naive-Bayes
+ Bagging?

## 3

# Inhalt
+ Anforderungen
+ Verarbeitungsgarantien
	+ Windows
	+ Watermarking
+ Statisitken finden 
	+ Sketches
	+ Count-Min-Sketch
+ Modellbildung
	+ VFDT

# Lernen
+ Stream Processing Anforderungen
+ Unterschied Event <=> Complex Event => Aggregierte Daten; Ergebnis der CEP-Engine
+ wie kann eine end-to-end exactly-once Verarbeitung gewährleistet werden?
	=> 

Ausgabemodi
	+ Complete: Komplette Ergebenistabelle wird geschrieben
	+ Append: Nur neue Windows
	+ Update: Nur Updates

Late-Data-Handling
	+ Append: Es wird auf das Watermark gewartet 
	+	+ Aggregation aller daten innerhalb der event-time + watermark
		+ Sobald das Watermark überschritten => Ausgabe des Windows
	+ Update: Window wird geschrieben
		+ Sobald neue Daten eintreffen, die in das Window passen
		=> Update des windows 		

+ Welche Probleme können aus unterschiedlicher Event und Processing Time entstehen?

+ BasicCounting anwenden
	+ ist er überhaupt fehleranfällig?
	=> Werden nicht alle einsen bedacht?
	=> Aber der letzte Bin enthälht ggf. Daten, die nicht mehr zum aktullen WIndow gehören (> n)

+ Was sind Sketches und deren Vorteil?
	+ In Memory-Datenstrukturen die ein Menge von Datensätzenr epräsentieren
	+ Sie beantworten Queries approximativ
	+ Sie haben immer eine fest definierte größe



+ Count-Min Sketch
	+ Allgemein
	+ Warum muss der kleinste Matrixeintrag gewählt werden?
		=> Weil es vermutlich viele Kollisionen gibt, daher die mit den wahrscheinlich wenigsten Kollisionen genommen.

+ Hoeffding-Bound
	+ Obergrenze für die Wahrschenilichkeit, dass ein Summe von Zufallsvariablen von ihrem Erwartungseert abweicht)

+ Was ist der Unterschied zwischen One shot vs one pass
	+ One Pass => daten nur einaml sichtbar
		=> Kein Median berechenbar


# Notes
Konzepte
	+ Event-Condition-Action
		+ Reaktion auf Änderungen von Datensätzen
	+ Query-Trigger-Stop
		+ Permanente Abfragen
	+ Publish Subscribe
		+ Event erzeuger und consumer entkoppelt
	+ CEP
		+ Sammeln, Filtern aggregieren.. Daten aus kontinuierlichen Datenströmen
		+ Ziel ist Realtime 
		+ Data is run through queries, rather than queries through data
+ Stream Processing Systeme
	+ flexibel, skalierbar, Fehler-tolerant, realtime

+ Operatoren
	+ Transformation von Daten-Tupeln
	+ Generierung von neuen Daten

Windows
	+ Dienen dazu, die Reichweite eine sOperators zu definieren.
	+ Zeit oder Anzahl von Events
	+ tumbling
	+ sliding
+ Event-Time vs Process-Time TODO: Blatt
	+ Single Hit, Follow-up hit, recurring-hit

+ Statistiken
	+ Inkrementell
		+ Werte wie Mittelwert,Varianz und Schiefe können inkrementell berechnet werden
		=> Probleme bei anderen Ansätzen: Histogramme
		=> Basic Counting
			Ziel: Genauigkeit gewährleisten, Speicherverbrauch klein halten
			+ Um nicht alle Werte zu speichern, werden diese in Bins zusammengefasst
			
	+ Sampling (Stichproben)
		+ Nachteile
			+ Verlustbehaftet
			+ Je nach sampling-strategie können bias und fehler entstehen
	+ Sketches
		+ Haben immer den gleichen Speicherbedarf, unahängig von Datenmenge 
	+ Count-Min Sketch
		+ Schnelle Beantwortung von Anfragen bzgl. Häufigkeiten (Approximativ)
		+ Größe der Matrix hängt von den Anforderungen ab
		
		+ Bestimmung eines maximalen Fehlers
		+ Generierung von Hash-Funktionen (d=Anzahl Zeilen), die Wertebreich M auf Wertebereich w abbilden.
		+ 	

+ VFDT
	+ Annahmen
		+ Jedes Attribut nimmt einen fest definierten Wertebreich an
		+ Anzahl der Klassen bekannt

	+ Für jedes Tupel wird entschieden, ob und ja wie Knoten gesplittet werden (Hoeffding-Bound)
	+ Null-Attribut, dass die Entscheidung "kein Split" wiederspiegelt.
	+ Split nur wenn:
		+ Information gain aller Attribute höher als der des Null-Attributs
		+ Information Gain des gewählten Attributes um Schwellwert höher als Second-Best attribut.
		=> Schwellwert-Berechnung
			+ Anzahl der Klassen, bisherige Tupel
	+ Scoring: Jedes Tupel erhält die Klasse mit den meisten Tupeln im jeweiligen Blatt

+ One-Shot vs One-Pass
	+ One-Shot: Gesamtmenge wird zur Partition des Wurzelknotens herangezogen
		+ Ausgewählter Datenbestand zu diskretem Zeitpunkt
	+ One-Pass: Partitionen werden sukzessive aufgebaut.
		=> Attribute, die für einen Split verwendet wurden, dürfen nicht noch einmal verwendet werden
		=> Null Attribut findet verwendung um splits zu begrenzen.
Concept Drift
	+ Plötzliche Änderungen in Datenbestand
	+ Target-Variable ist instabil und unterliegt externen Faktoren
		+ Saisonaler Anstieg von Sonnencreme-Verkäufen
	=> Modell muss modifiziert werden
	
	+ IncrementalOLIN
		+ Trainiert modell so lange, bis ein concept drift erkannt wird
		=> Start eines nuen Modells

## 4

# Inhalt
+ Cap-Theorem
+ Lambda
+ Kappa

# Lern
+ Was ist das CAP-Theorem?
=> Das Cap Theorem besagt, dass die 3 Eigenschaften Consitency, Availability und Partition Tolerance einer verteilten DB(!)-Architektur niemals zusammen erfüllt werden können.
=> Entweder man wählt Consistency over Availability => Daten sind immer konsistent, aber dafür hat man lange antwortzeiten/Wartezeiten
=> Oder Availability over Consistency => Man erhält immer eine Antwort, auch wenn diese nicht unbedingt korrekt ist
	

+ Wie umgeht die Lambda-Architektur die limitierungen des CAP-Theorems
=> Die Lambda Architektur stellt mehrere Layer vor, durch die die eintreffenden Daten verarbeitet werden.
	+ Der Batch-Layer sammelt die Daten und berechnet Anfragen. Dies passiert exakt, dauert aber auch dementsprechend lange
	+ Der Speed-Layer approximiert TODO: Noch nicht so gutes wissen
	+ Der Serving Layer ist die Schnittstelle zum User und stellt dem Nutzer je nach Availability Daten aus dem Speed oder Batch Layer zur Verfügung
	+ 

Batch View
	=> Je query; wird immer aktualisiert, bei neuen Daten
Speed Layer
	=> Kompoensiert hohe Latenzen, Inkrementelle Algorithmen auf Basiss der neusten Daten.

+ TODO: Was sind die Nachteile der Lambda-Architektur?
+ Verschiedene Programmierschnittstellen für Batch und Speed-Layer

+ Was macht die Kappa-Architektur besser?
=> Geht davon aus, dass die Daten durch den Speed-Layer und geeigenet Queue(kafka) alle schon vorhanden sind und daher kein Batch-layer benötigt wird.
 Die konkreten Daten können nur auf der Queue berechnet werden. 

+ TODO: Vorteile der Kappa-Architektur


# Blatt
+ Anforderungen


## 5

# Inhalt
+ Eigenschaften von Graphen
+ Muster in Graphen
+ Graphen-Algorithem
	+ Pagerank
	+ Community-Detection
	+ ..

## Community Detection

+ Clustering-Koeffizient
	+ Verhältnis von tatsächlichen und maximalen Kanten in der Nachbarschaft.

+ Globaler Clustering-Koeffizient
	+ Anzahl der Dreiecke
	+ Formel: Anzahl Dreiecke * 3 / Anzahl Tripel

+ Clustering-Koeffizient des Graphen
	+ Durchschnittlicher Clustering-Koeffizient

+ Clique: Alle Knoten sind mit allen anderen Knoten verbunden


# Blatt
+ Spektrum?
+ Formel => Cluster-Koeffizient 
	=> 2*kantenzahl / (knoten * (knoten - 1))
=> Degree Centrality = Durchschnittlicher Grad
=> Betweenness Centrality
=> Edge Betweeness = Anzahl der kürzesten Wege pro kante
+ PageRank
	+ (1-d) + d * (PR(i) + Pr(i+1)..)

