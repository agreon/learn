# CAP-Theorem
+ Verteilte Datenbanken können höchsten zwei der drei Größen erreichen
	+ Consistency
	+ Availability
	+ Partition-Tolerance 

Partitions-Toleranz sollte auf keinen Fall vernachlässigt werden
TODO: Was ist das?
=> Balance zwischen Availability und Consistency
+ Eventually Consitent
	+ Availability over Consistency
	=> Veraltete Reads und konflikte bei Writes möglich
+ Consistency over Availability
	+ Alle Nodes müssen gleichzeitig geupdated werden
	=> Wartezeiten
	
+ Append-Only
	+ Keine Datensätze geändert/überschrieben
	TODO:	
	=> Vorteile?
	=> Nachteile?
## Lernen
	=> Anforderungen (5)
	=> Anforderungen (8)

#lambda
=> Lösungsansatz für das Persistieren von Daten aus real-time Datenströmen

+ Kombination aus approximierten real-time daten und finalen korrekten werten


+ batch layer (masterdaten)
	+ append-only
	=> hohe latenzen
	=> korrektheit
	+ generiert (periodisch?) batch views und gibt sie weiter an serving-layer
	Aufgaben
		+ Speichern von nicht änderbarer, stetig wachsender Datenmenge
		+ Berechnung beliebiger Funktionen auf Datenmenge

	+ Batch View
		+ Ermöglichen es, gewünschte Ergebnisse von Anfragen schnell zu erhalten, ohne Datenbestand neu zu analysieren
	
+ Serving-layer
=> stellt batch-views den queries zur verfügung
+ TODO: Warum Random access? (17)

+ Speed layer
+ kompensiert Latenz des Batch-Layers
	=> geringe latenzen
	=> probleme bei korrektheit
	+ generiert realtime-views aus kürzlich eingetroffenen Daten
	+ verwendet Inkrementelle Algorithmen
+ Sobald Batch-Layer Daten berechnet hat, werden Realtime-Views gelöscht

Vorteil
Realtime und batch-views ergänzen sich zu korrekter, schneller datensenke

Nachteile
=> Unterschiedliche Programmierschnittstellen bei speed und batch-layer
	+ Muss doppelt implementiert werden

# Kappa-Architektur
+ Verschmelzung von Batch layer und Speed-Layer 
=> Komplette Logik in einer Streaming-Engine realisiert


