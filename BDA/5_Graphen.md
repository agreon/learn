# Graphen
Graphen dienen zur Darstellungen von Objekten die untereinander in Beziehung stehen.
Analyse geschieht zum Auffinden von Annomalien oder zusammenhängenden Communities

# Spektrum
+ Geordente Liste der Eigenwerte der Adjazentmatrix
=> Spektralradius => größter Eigenwert
+ 

# Bipartiter Graph
+ Spektrum besteht aus (wrz(n*m), 0, ..., 0, -wrz(n*m))


# Dichte
+ Verhältnis der Kanten zu den maximal möglichen Kanten
+ Wie viele Konten enthält ein voll-zusammenhängender Graph?
	=> n*n? => TODO: Überprüfen
+ Ein Baum besteht aus N-1 Kanten
+ Cool: Bei mehreren unabhängigen(disjunkten) Bäumen ist die Anzahl der Bäume = Knoten - Kanten

# Pfad
=> Sequenz von Kanten, die ein Paar von Knoten verbindet
+ Distanz: Länge des kürzesten Pfades zwischen 2 Knoten
+ Durchschnittliche Pfadlänge: Durchschnitt aller Knotenpaare
+ Durchmesser des Graphen: die größte Distanz aller Knotenpaare

# Grad
+ Anzahl der Kanten, die ein Knoten besitzt
+ maximaler Grad: Knoten mit den meisten Verbindungen

# Knoten-Zentralität
=> Welcher Knoten ist geeignet, das Zentrum des Graphen darzustellen?
+ Höchster Grad
+ Betweenness centrality
	+ Häufigkeit, mit der ein Knoten im kürzesten Pfad zwei anderer Knoten enthalten ist
+ Closure Centrality


# Zusammenhangskomponenten
+ schwache und stark! transitivität (18)
+ Message Passing-Algorithmus
	+ theoretisch endlose iteration bis keine Änderung mehr vorgenommen wird
	+ konten nehmen in jeder iteration die geringste ID ihrer Nachbarn an
+ One-Pass Streaming
	+ Jedes Knoten-Paar wird in einem Stream verarbeitet


# Muster (23)
## Statische Graphen
	+  Power-Law /Potenzgesetz
		+ Logarithmischer Anstieg?
		+ skaleninvariant
		+ Wenige mit viel, viele mit wenig
		+ Beispiele
			+ Internet
				+ Backbones haben gigantischen Grad, heimnetze geringen
			+ Einkommen der Bevölkerung
			+ Dateigrößen (Google Dateisstem?)

	+ Eigenwert Potenzgesetz
		+ TODO
	+ Schmale Durchmesser
		+ In der Regel kleine Durchmesser
			+ Internet 19
			+ Soziale Netzwerke nur 6

	+ Dreiecks-Potzengesetze
		+ Freunde von Freunden werden Freunde
		+ Participation Law
			+ Verteilung von Dreiecken folgt Portenzgesetz
		+ Degree Particiaption
			+ Korrelation zwischen Grad eines Knoten und Anzahl der Dreiecke

## Dynamische Graphen
	+ Schrumfender Durchmesser
		+ "neigt dazu, dass der durchmesser auf dauer geringer wird"
	+ Verdichtungs-Potenzgesetz
		+ Das Wachstum der Kantenfolgt einem Potenzgesetz
		+ super-lineare bzeiehung: Potenzweert > 1

	+ Gelling-Point
		+ Nachdem der Durchmesser einen Höhepunkt erreicht hat, stabilisiert er sich
		+ Wie zu erklären? 
			+ mehrere kleine komponenten verschmelzen zu einer sehr großen => Sehr wahrscheinlich, dass neue knoten anschlluss finden

## Gewichtete Graphen
	+ Snapshot Power-Laws
		+ TODO
	+ Potenzgesetz Gewichte
		+ Das totale Gewicht des Graphen entspricht meistens = Alle  Kanten ^1-1.5
		+ 
